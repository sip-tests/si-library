@extends('../index')
@section('title','Register')
@section('description','Please fill the form to register.')

@section('content')
<div class=" pt-8 pb-2 text-base leading-6 space-y-4 text-gray-700 sm:text-lg sm:leading-7">
    <form action="{{route('authRegister')}}" method="POST"> 
        @csrf
        @method('post')
        <div class="relative pb-8">
            <input autocomplete="off" id="name" name="name" type="text"
                class="peer placeholder-transparent bg-transparent h-10 w-full border-b-2 border-gray-300 text-gray-900 focus:outline-none focus:borer-rose-600"
                placeholder="Full name" value="{{ old('name') }}"/>
                @error('name')
                <span>{{$message}}</span>
            @enderror
            <label for="name"
                class="absolute left-0 -top-3.5 text-gray-600 text-sm peer-placeholder-shown:text-base peer-placeholder-shown:text-gray-440 peer-placeholder-shown:top-2 transition-all peer-focus:-top-3.5 peer-focus:text-gray-600 peer-focus:text-sm">Full Name</label>
        </div>
        <div class="relative pb-8">
            <input autocomplete="off" id="email" name="email" type="email"
                class="peer placeholder-transparent bg-transparent h-10 w-full border-b-2 border-gray-300 text-gray-900 focus:outline-none focus:borer-rose-600"
                placeholder="Email address" value="{{ old('email') }}"/>
                @error('email')
                    <span>{{$message}}</span>
                @enderror
            <label for="email"
                class="absolute left-0 -top-3.5 text-gray-600 text-sm peer-placeholder-shown:text-base peer-placeholder-shown:text-gray-440 peer-placeholder-shown:top-2 transition-all peer-focus:-top-3.5 peer-focus:text-gray-600 peer-focus:text-sm">Email
                Address</label>
        </div>
        <div class="relative pb-8">
            <input autocomplete="off" id="address" name="address" type="text"
                class="peer placeholder-transparent bg-transparent h-10 w-full border-b-2 border-gray-300 text-gray-900 focus:outline-none focus:borer-rose-600"
                placeholder="Address" value="{{ old('address') }}"/>
                @error('address')
                    <span>{{$message}}</span>
                @enderror
            <label for="address"
                class="absolute left-0 -top-3.5 text-gray-600 text-sm peer-placeholder-shown:text-base peer-placeholder-shown:text-gray-440 peer-placeholder-shown:top-2 transition-all peer-focus:-top-3.5 peer-focus:text-gray-600 peer-focus:text-sm">
                Address</label>
        </div>
        <div class="relative pb-8">
            <input autocomplete="off" id="phone" name="phone" type="text"
                class="peer placeholder-transparent bg-transparent h-10 w-full border-b-2 border-gray-300 text-gray-900 focus:outline-none focus:borer-rose-600"
                placeholder="Phone Number" value="{{ old('phone') }}"/>
                @error('phone')
                    <span>{{$message}}</span>
                @enderror
            <label for="phone"
                class="absolute left-0 -top-3.5 text-gray-600 text-sm peer-placeholder-shown:text-base peer-placeholder-shown:text-gray-440 peer-placeholder-shown:top-2 transition-all peer-focus:-top-3.5 peer-focus:text-gray-600 peer-focus:text-sm">Phone
                Number</label>
        </div>
        <div class="relative">
            <input autocomplete="off" id="password" name="password" type="password"
                class="peer placeholder-transparent bg-transparent h-10 w-full border-b-2 border-gray-300 text-gray-900 focus:outline-none focus:border-rose-600"
                placeholder="Password" value="{{ old('password') }}"/>
                @error('password')
                    <span>{{$message}}</span>
                @enderror
            <label for="password"
                class="absolute left-0 -top-3.5 text-gray-600 text-sm peer-placeholder-shown:text-base peer-placeholder-shown:text-gray-440 peer-placeholder-shown:top-2 transition-all peer-focus:-top-3.5 peer-focus:text-gray-600 peer-focus:text-sm">Password</label>
            <button id="togglePassword" type="button" class="absolute right-2 top-3.5 text-gray-600 text-sm focus:outline-none">
                <i id="eye-icon">
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor"
                        class="w-4 h-4">
                        <path stroke-linecap="round" stroke-linejoin="round"
                            d="M2.036 12.322a1.012 1.012 0 010-.639C3.423 7.51 7.36 4.5 12 4.5c4.638 0 8.573 3.007 9.963 7.178.07.207.07.431 0 .639C20.577 16.49 16.64 19.5 12 19.5c-4.638 0-8.573-3.007-9.963-7.178z" />
                        <path stroke-linecap="round" stroke-linejoin="round" d="M15 12a3 3 0 11-6 0 3 3 0 016 0z" />
                    </svg>
                </i>
                <i id="eye-slash-icon" style="display: none;">
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor"
                        class="w-4 h-4">
                        <path stroke-linecap="round" stroke-linejoin="round"
                            d="M3.98 8.223A10.477 10.477 0 001.934 12C3.226 16.338 7.244 19.5 12 19.5c.993 0 1.953-.138 2.863-.395M6.228 6.228A10.45 10.45 0 0112 4.5c4.756 0 8.773 3.162 10.065 7.498a10.523 10.523 0 01-4.293 5.774M6.228 6.228L3 3m3.228 3.228l3.65 3.65m7.894 7.894L21 21m-3.228-3.228l-3.65-3.65m0 0a3 3 0 10-4.243-4.243m4.242 4.242L9.88 9.88" />
                    </svg>
    
                </i>
            </button>
        </div>
        <div class="flex justify-center items-center pt-5">
            <button type="submit" class=" bg-slate-950 text-white w-full py-1 hover:bg-slate-700">Register</button>
        </div>
    </form>
    <div class="text-center">
        <small>Already have an account? <a href="/" class="font-bold hover:text-slate-500">Login</a></small>
    </div>
</div>
@endsection