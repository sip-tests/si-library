@extends('visitor.layouts.index')

@section('content')
    <section class="px-5 md:px-0 w-full md:w-11/12 mx-auto">
        <div class="container mx-auto md:w-11/12">
            <h1 class="text-xl py-5 font-medium">My Books</h1>
            <hr class="bg-slate-200 mb-3">
            @if(session('Error'))
                <div
                    class="mb-4 rounded-lg bg-red-100 px-6 py-3 text-base text-red-700"
                    role="alert">
                    {{ session('Error') }}
                </div>
            @endif

            @if(session('Success'))
                <div
                    class="mb-4 rounded-lg bg-green-100 px-6 py-3 text-base text-green-700"
                    role="alert">
                    {{ session('Success') }}
                </div>
            @endif
            @if($borrowedBooks->isEmpty())
                <p class="flex justify-center text-center italic text-xl">You don't have books</p>
            @else
            <div class="grid grid-cols-2 sm:grid-cols-3 md:grid-cols-5 gap-5">
                  @foreach ($borrowedBooks as $item)
                      <div class="relative flex-col text-gray-700 bg-white shadow-md bg-clip-border" id="card-shadow">
                          <div class="overflow-hidden m-3 border h-max border-black text-white bg-blue-gray-500 bg-clip-border shadow-blue-gray-500/40">
                              <img
                                src="{{url($item->book->cover)}}"
                                alt="img-blur-shadow"
                                class="w-full"
                                layout="fill"
                              />
                          </div>
                          <div class="p-6">
                              <!-- Display book details here -->
                          </div>
                          <div class="absolute bottom-3 right-3">
                              <form action="{{ route('returnBook', ['book' => $item->book_id]) }}" method="POST">
                                  @csrf
                                  <button
                                      class="bg-black py-3 px-6 text-center align-middle font-sans text-xs font-bold uppercase text-white shadow-md shadow-slate-400/50 transition-all"
                                      type="submit"
                                      data-ripple-light="true"
                                      onclick="return confirm('Are you sure you want to return this book?');"
                                  >
                                      Return
                                  </button>
                              </form>
                          </div>
                      </div>
                  @endforeach
                </div>
                @endif
        </div>
        
    </section>
@endsection