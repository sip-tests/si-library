@extends('visitor.layouts.index')

@section('content')
    <section class="px-5 md:px-0 w-full md:w-11/12 mx-auto pb-10">
        <div class="container mx-auto">
          @if(session('Error'))
              <div
                  class="mb-4 rounded-lg bg-red-100 px-6 mt-10 py-3 text-base text-red-700"
                  role="alert">
                  {{ session('Error') }}
              </div>
          @endif

          @if(session('Success'))
              <div
                  class="mb-4 rounded-lg bg-green-100 mt-10 px-6 py-3 text-base text-green-700"
                  role="alert">
                  {{ session('Success') }}
              </div>
          @endif
          <div class="flex flex-wrap justify-between w-full">
            <h1 class="text-xl py-5 font-medium">Books</h1>
            <div class='max-w-lg my-3 '>
              <div class="flex items-center w-full h-12 border border-slate-200 focus-within:shadow-lg bg-white overflow-hidden">
                  <div class="grid place-items-center h-full w-12 text-gray-300">
                      <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                      <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z" />
                      </svg>
                  </div>
                  <input
                    class="peer h-full w-full outline-none text-sm text-gray-700 pr-2"
                    type="text"
                    name="query"
                    id="searchBooks"
                    value="{{ request('query') }}"
                    placeholder="Search books.." /> 
              </div>
          </div>
          </div>
            <hr class="bg-slate-200 mb-3">
            <div class="grid grid-cols-2 sm:grid-cols-3 md:grid-cols-5 gap-5">
              @foreach ($books as $book)    
              <div class="relative flex-col text-gray-700 bg-white shadow-md bg-clip-border" id="card-shadow">
                    <div class="overflow-hidden m-3 border h-max border-black text-white bg-blue-gray-500 bg-clip-border shadow-blue-gray-500/40">
                      <img
                      src="{{url($book->cover)}}"
                      alt="img-blur-shadow"
                      class="w-full"
                      layout="fill"
                      />
                    </div>
                    <div class="p-6">
                    </div>
                    <div class="absolute bottom-3 right-3">
                      <form action="{{ route('borrowBook', ['book' => $book->id]) }}" method="POST">
                        @csrf
                      <button
                        class="bg-black py-3 px-6 text-center align-middle font-sans text-xs font-bold uppercase text-white shadow-md shadow-slate-400/50 transition-all"
                        type="submit"
                        data-ripple-light="true"
                        onclick="return confirm('Are you sure you want to borrow this book?');"
                      >
                        Borrow
                      </button>
                    </form>
                    </div>
              </div>
            @endforeach
            </div>
        </div>
    </section>
    
    
@endsection