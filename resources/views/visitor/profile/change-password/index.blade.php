@extends('visitor.layouts.index')

@section('content')
    <section class="px-5 md:px-0 w-full md:w-6/12 mx-auto">
        <div class="container mx-auto md:w-11/12">
            <div class="flex-auto px-2 lg:px-10 py-10 pt-0">
                <h1 class="text-xl py-5 font-medium">Change password</h1>
                @if(session('Error'))
                    <div
                        class="mb-4 rounded-lg bg-red-100 px-6 py-3 text-base text-red-700"
                        role="alert">
                        {{ session('Error') }}
                    </div>
                @endif

                @if(session('Success'))
                    <div
                        class="mb-4 rounded-lg bg-green-100 px-6 py-3 text-base text-green-700"
                        role="alert">
                        {{ session('Success') }}
                    </div>
                @endif
                <hr class="bg-slate-200 mb-3">
                <form method="POST" action="{{ route('profile.updatePassword', $profile->id) }}">
                    @csrf
                    @method('PUT')
                  <div class="flex flex-wrap">
                        <div class="w-full px-2">
                            <div class="relative w-full mb-3">
                                <label class="block text-black font-bold mb-2">
                                    Current password
                                </label>
                                <input name="old_password" type="password" class="border border-black px-3 py-3 placeholder-black text-black bg-white text-md shadow focus:outline-none focus:ring w-full" value="{{ old('old_password') }}">
                                @error('old_password')
                                    <span>{{$message}}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="w-full px-2">
                            <div class="relative w-full mb-3">
                                <label class="block text-black font-bold mb-2">
                                New password
                                </label>
                                <input name="new_password" type="password" class="border border-black px-3 py-3 placeholder-black text-black bg-white text-md shadow focus:outline-none focus:ring w-full" value="{{ old('new_password') }}">
                                @error('new_password')
                                    <span>{{$message}}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="w-full px-2">
                            <div class="relative w-full mb-3">
                                <label class="block text-black font-bold mb-2">
                                Verify password
                                </label>
                                <input name="verify_password" type="password" class="border border-black px-3 py-3 placeholder-black text-black bg-white text-md shadow focus:outline-none focus:ring w-full" value="{{ old('verify_password') }}">
                                @error('verify_password')
                                    <span>{{$message}}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="flex justify-end px-2 pt-4">
                        <button type="submit" class="bg-black text-white px-6 py-3 hover:bg-slate-800 shadow-slate-500/50">Edit</button>
                    </div>
                  </div>
                </form>
              </div>
        </div>
    </section>
@endsection