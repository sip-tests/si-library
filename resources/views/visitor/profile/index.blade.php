@extends('visitor.layouts.index')

@section('content')
    <section class="px-5 md:px-0 w-full md:w-6/12 mx-auto">
        <div class="container mx-auto md:w-11/12 py-10">
            @if(session('Error'))
                <div
                    class="mb-4 rounded-lg bg-red-100 px-6 py-3 text-base text-red-700"
                    role="alert">
                    {{ session('Error') }}
                </div>
            @endif

            @if(session('Success'))
                <div
                    class="mb-4 rounded-lg bg-green-100 px-6 py-3 text-base text-green-700"
                    role="alert">
                    {{ session('Success') }}
                </div>
            @endif
            <div class="flex justify-normal md:justify-center">
                <svg xmlns="http://www.w3.org/2000/svg" width="120" height="120" viewBox="0 0 61.8 61.8" id="avatar"><g data-name="Layer 2"><g data-name="—ÎÓÈ 1"><circle cx="30.9" cy="30.9" r="30.9" fill="#ffc200"></circle><path fill="#677079" fill-rule="evenodd" d="M52.587 52.908a30.895 30.895 0 0 1-43.667-.291 9.206 9.206 0 0 1 4.037-4.832 19.799 19.799 0 0 1 4.075-2.322c-2.198-7.553 3.777-11.266 6.063-12.335 0 3.487 3.265 1.173 7.317 1.217 3.336.037 9.933 3.395 9.933-1.035 3.67 1.086 7.67 8.08 4.917 12.377a17.604 17.604 0 0 1 3.181 2.002 10.192 10.192 0 0 1 4.144 5.22z"></path><path fill="#f9dca4" fill-rule="evenodd" d="m24.032 38.68 14.92.09v3.437l-.007.053a2.784 2.784 0 0 1-.07.462l-.05.341-.03.071c-.966 5.074-5.193 7.035-7.803 8.401-2.75-1.498-6.638-4.197-6.947-8.972l-.013-.059v-.2a8.897 8.897 0 0 1-.004-.207c0 .036.003.07.004.106z"></path><path fill-rule="evenodd" d="M38.953 38.617v4.005a7.167 7.167 0 0 1-.095 1.108 6.01 6.01 0 0 1-.38 1.321c-5.184 3.915-13.444.704-14.763-5.983z" opacity=".11"></path><path fill="#f9dca4" fill-rule="evenodd" d="M18.104 25.235c-4.94 1.27-.74 7.29 2.367 7.264a19.805 19.805 0 0 1-2.367-7.264zM43.837 25.235c4.94 1.27.74 7.29-2.368 7.263a19.8 19.8 0 0 0 2.368-7.263z"></path><path fill="#ffe8be" fill-rule="evenodd" d="M30.733 11.361c20.523 0 12.525 32.446 0 32.446-11.83 0-20.523-32.446 0-32.446z"></path><path fill="#8a5c42" fill-rule="evenodd" d="M21.047 22.105a1.738 1.738 0 0 1-.414 2.676c-1.45 1.193-1.503 5.353-1.503 5.353-.56-.556-.547-3.534-1.761-5.255s-2.032-13.763 4.757-18.142a4.266 4.266 0 0 0-.933 3.6s4.716-6.763 12.54-6.568a5.029 5.029 0 0 0-2.487 3.26s6.84-2.822 12.54.535a13.576 13.576 0 0 0-4.145 1.947c2.768.076 5.443.59 7.46 2.384a3.412 3.412 0 0 0-2.176 4.38c.856 3.503.936 6.762.107 8.514-.829 1.752-1.22.621-1.739 4.295a1.609 1.609 0 0 1-.77 1.214c-.02.266.382-3.756-.655-4.827-1.036-1.07-.385-2.385.029-3.163 2.89-5.427-5.765-7.886-10.496-7.88-4.103.005-14 1.87-10.354 7.677z"></path><path fill="#434955" fill-rule="evenodd" d="M19.79 49.162c.03.038 10.418 13.483 22.63-.2-1.475 4.052-7.837 7.27-11.476 7.26-6.95-.02-10.796-5.6-11.154-7.06z"></path><path fill="#e6e6e6" fill-rule="evenodd" d="M36.336 61.323c-.41.072-.822.135-1.237.192v-8.937a.576.576 0 0 1 .618-.516.576.576 0 0 1 .619.516v8.745zm-9.82.166q-.622-.089-1.237-.2v-8.711a.576.576 0 0 1 .618-.516.576.576 0 0 1 .62.516z"></path></g></g></svg>
            </div>
            @foreach ($profile as $item)    
            <div class="flex flex-wrap justify-between pt-10 ">
                <div>
                    <p class="text-2xl font-semibold">{{$item->name}}</p>
                    <p class="pb-5">{{$item->email}}</p>
                </div>
                <div class="flex pt-2 pb-8">
                    <div class="flex justify-center gap-3">
                        <a href="/visitor/profile/{{$item->id}}/change-password" class="">
                            <span class="hover:bg-slate-300 bg-slate-200 p-2 border border-slate-700">
                                Change Password
                            </span>
                        </a>
                        <a href="/visitor/profile/{{$item->id}}/edit" class="">
                            <span class="hover:bg-yellow-300 bg-yellow-200 p-2 border border-yellow-700">
                                Edit
                            </span>
                        </a>
                    </form>
                </div>
                </div>
            </div>
            @endforeach
            <div class="flex flex-wrap gap-3">
                <div class="flex rounded-lg bg-green-100 px-3 py-2">
                    <svg xmlns="http://www.w3.org/2000/svg" width="26" height="26" viewBox="0 0 24 24" fill="none" stroke="#15803d" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="lucide lucide-graduation-cap"><path d="M22 10v6M2 10l10-5 10 5-10 5z"/><path d="M6 12v5c3 3 9 3 12 0v-5"/></svg>
                    <p class="px-3 text-lg text-green-700 font-medium">Student</p>
                </div>
                <div class="flex rounded-lg bg-yellow-100 px-3 py-2">
                    <svg xmlns="http://www.w3.org/2000/svg" width="26" height="26" viewBox="0 0 24 24" fill="none" stroke="#ca8a04" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="lucide lucide-book-heart"><path d="M4 19.5v-15A2.5 2.5 0 0 1 6.5 2H20v20H6.5a2.5 2.5 0 0 1 0-5H20"/><path d="M16 8.2C16 7 15 6 13.8 6c-.8 0-1.4.3-1.8.9-.4-.6-1-.9-1.8-.9C9 6 8 7 8 8.2c0 .6.3 1.2.7 1.6h0C10 11.1 12 13 12 13s2-1.9 3.3-3.1h0c.4-.4.7-1 .7-1.7z"/></svg>
                    <p class="px-3 text-lg text-yellow-700 font-medium">Visitor</p>
                </div>
            </div>
            <hr class="mt-5 mb-3">
            <h1 class="text-xl font-medium">History</h1>
            <table class="min-w-full text-center text-md w-full" >
                <thead class="border-b">
                    <tr>
                        <th scope="col" class="px-6 py-4 text-left">Title</th>
                        <th scope="col" class="px-6 py-4">Borrow Date</th>
                        <th scope="col" class="px-6 py-4">Return Date</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($borrowHistory as $item)    
                        <tr class="border-b">
                            <td class="px-6 py-4 text-left">{{$item->book->title}}</td>
                            <td class="px-6 py-4">{{$item->borrowDate}}</td>
                            <td class="px-6 py-4">{{$item->actualReturnDate}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </section>
@endsection