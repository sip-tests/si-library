@extends('visitor.layouts.index')

@section('content')
    <section class="px-5 md:px-0 w-full md:w-11/12 mx-auto">
        <div class="container mx-auto md:w-11/12">
            <h1 class="text-xl py-5 font-medium">History</h1>
            <div class="flex flex-col">
                <div class="overflow-x-auto sm:-mx-6 lg:-mx-8">
                    <div class="w-full py-2 px-5 lg:px-0" >
                        <div class="overflow-hidden border border-slate-200">
                            <table class="min-w-full text-center text-md w-full" >
                                <thead class="border-b bg-black text-white">
                                    <tr>
                                        <th scope="col" class="px-6 py-4">No</th>
                                        <th scope="col" class="px-6 py-4">Title</th>
                                        <th scope="col" class="px-6 py-4">Borrow Date</th>
                                        <th scope="col" class="px-6 py-4">Return Date</th>
                                        <th scope="col" class="px-6 py-4">Due</th>
                                        <th scope="col" class="px-6 py-4">Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="border-b">
                                        <td class="px-6 py-4 font-medium">1</td>
                                        <td class="px-6 py-4 text-left">Introduction to JavaScript</td>
                                        <td class="px-6 py-4">2023-10-15</td>
                                        <td class="px-6 py-4">2023-10-30</td>
                                        <td class="px-6 py-4">15 days</td>
                                        <td class="px-6 py-4 font-semibold text-yellow-500"><span class="bg-yellow-100 px-4 rounded-full">Borrowed</span></td>
                                    </tr>
                                    <tr class="border-b dark-border-neutral-500 bg-neutral-100">
                                        <td class="px-6 py-4 font-medium">2</td>
                                        <td class="px-6 py-4 text-left">The Great Gatsby</td>
                                        <td class="px-6 py-4">2023-10-10</td>
                                        <td class="px-6 py-4">2023-11-05</td>
                                        <td class="px-6 py-4">26 days</td>
                                        <td class="px-6 py-4 font-semibold text-red-500"><span class="bg-red-100 px-4 rounded-full">Overdue</span></td>
                                    </tr>
                                    <tr class="border-b dark-border-neutral-500">
                                        <td class="px-6 py-4 font-medium">3</td>
                                        <td class="px-6 py-4 text-left">To Kill a Mockingbird</td>
                                        <td class="px-6 py-4">2023-10-20</td>
                                        <td class="px-6 py-4">2023-11-15</td>
                                        <td class="px-6 py-4">26 days</td>
                                        <td class="px-6 py-4 font-semibold text-green-500"><span class="bg-green-100 px-4 rounded-full">Returned</span></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection