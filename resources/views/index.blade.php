<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" type="text/css" href="{{ url('/css/style.css') }}" />
  @vite('resources/css/app.css')
</head>
<body class="bg-slate-100 font-serif" style="background-image: url(../images/background.jpg); background-size: cover; background-repeat: no-repeat;">
    
    <div class="container w-full mx-auto">
        <div class="flex justify-center items-center h-screen">
            <div class="md:mx-5 md:px-5 w-10/12 sm:w-8/12 md:w-6/12 xl:w-4/12 lg:px-8 bg-white p-8 md:p-10" id="form-shadow">
                <div>
                    <h1 class=" text-3xl font-bold pb-2">@yield('title')</h1>
                    <h2 class="text-lg font-medium text-slate-700">@yield('description')</h2>
                </div>
                <div class="">
                    @yield('content')
                </div>
            </div>
        </div>
    </div>
    <script>
        const passwordInput = document.getElementById("password");
        const togglePasswordButton = document.getElementById("togglePassword");
        const eyeIcon = document.getElementById("eye-icon");
        const eyeSlashIcon = document.getElementById("eye-slash-icon");

        togglePasswordButton.addEventListener("click", function () {
            if (passwordInput.type === "password") {
                passwordInput.type = "text";
                eyeIcon.style.display = "none";
                eyeSlashIcon.style.display = "block";
            } else {
                passwordInput.type = "password";
                eyeIcon.style.display = "block";
                eyeSlashIcon.style.display = "none";
            }
        });
    </script>
</body>
</html>