@extends('admin.layouts.index')
@section('home','text-semibold')

@section('content')
    <div class="w-full text-2xl md:text-6xl my-2 md:my-5">
      <div class="w-10/12 mx-auto py-2 md:py-5 font-bold text-transparent text-center bg-clip-text bg-gradient-to-r from-purple-400 to-pink-600">
        <h2>Admin Homepage</h2>
      </div>
    </div>
    <section class="px-5 md:px-0 w-full md:w-11/12 mx-auto pb-10">
        <div class="container mx-auto">
            <div class="border-b border-slate-300 w-full py-5"></div>
            <h1 class="text-xl py-5 font-medium">Books ({{ $count}})</h1>
            <div class="grid grid-cols-2 sm:grid-cols-3 md:grid-cols-5 gap-5 ">
                @foreach ($books as $book)    
                  <div class="relative flex-col text-gray-700 bg-white shadow-md bg-clip-border" id="card-shadow">
                        <div class="overflow-hidden m-3 border h-max border-black text-white bg-blue-gray-500 bg-clip-border shadow-blue-gray-500/40">
                          <img
                            src="{{url($book->cover)}}"
                            alt="img-blur-shadow"
                            class="w-full"
                            layout="fill"
                          />
                        </div>
                        <div class="p-6">
                        </div>
                        <div class="absolute bottom-5 right-3">
                          <a
                            href="/admin/books/{{$book->id}}/details" class="bg-black py-3 px-6 text-center align-middle font-sans text-xs font-bold uppercase text-white shadow-md shadow-slate-400/50 transition-all"
                            data-ripple-light="true"
                          >
                            Details
                          </a>
                        </div>
                  </div>
                @endforeach
            </div>
        </div>
        
    </section>
    
    
@endsection