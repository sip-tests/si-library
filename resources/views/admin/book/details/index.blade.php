@extends('admin.layouts.index')
@section('home','text-semibold')

@section('content')
    <section class="px-5 md:px-0 w-full md:w-11/12 mx-auto pb-10">
        <div class="container mx-auto">
            <div class="border-b border-slate-300 w-full py-5 mb-5"></div>
            <h1 class="text-xl py-5 font-medium text-center">Book Details</h1>
            @foreach ($books as $item)    
                <div
                    class="flex flex-col bg-white mx-auto md:flex-row" id="card-shadow">
                    <div class="overflow-hidden m-3 border h-max border-black text-white bg-blue-gray-500 bg-clip-border shadow-blue-gray-500/40">
                        <img
                          src="{{url($item->cover)}}"
                          alt="img-blur-shadow"
                          class="w-full"
                          layout="fill"
                        />
                      </div>
                    <div class="flex flex-col w-fit justify-start p-6">
                        <h5
                        class="mb-2 text-xl font-medium ">
                        {{$item->title}}
                        </h5>
                        <p class="mb-4 text-base ">
                            {{$item->author}}
                            </p>
                            <p class="mb-4 text-base ">
                                {{$item->isbn}}
                                </p>
                        <p class="mb-4 text-base ">
                        {{$item->description}}
                        </p>
                    </div>
                </div>
            @endforeach
            
        </div>
        
    </section>
    
    
@endsection