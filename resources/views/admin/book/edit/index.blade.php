@extends('admin.layouts.index')

@section('content')
    <section class="px-5 md:px-0 w-full md:w-11/12 mx-auto">
        <div class="container mx-auto md:w-11/12">
            <div class="flex-auto px-2 lg:px-10 py-10 pt-0">
                <h1 class="text-xl py-5 font-medium">Edit Book</h1>
                <form method="POST" action="{{ route('books.update', $book->id) }}">
                    @csrf
                    @method('PUT')
                  <div class="flex flex-wrap">
                    <div class="w-full lg:w-6/12 px-2">
                      <div class="relative w-full mb-3">
                        <label class="block uppercase text-black font-bold mb-2">
                          Title
                        </label>
                        <input name="title" type="text" class="border border-black px-3 py-3 placeholder-black text-black bg-white text-md focus:outline-none focus:ring w-full" value="{{ $book->title }}">
                        @error('title')
                            <span>{{$message}}</span>
                        @enderror
                      </div>
                    </div>
                    <div class="w-full lg:w-6/12 px-2">
                      <div class="relative w-full mb-3">
                        <label class="block uppercase text-black font-bold mb-2">
                          Author
                        </label>
                        <input name="author" type="text" class="border border-black px-3 py-3 placeholder-black text-black bg-white text-md shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150" value="{{ $book->author }}">
                        @error('author')
                            <span>{{$message}}</span>
                        @enderror
                    </div>
                    </div>
                    <div class="w-full lg:w-6/12 px-2">
                      <div class="relative w-full mb-3">
                        <label class="block uppercase text-black font-bold mb-2">
                          ISBN
                        </label>
                        <input name="isbn" type="text" class="border border-black px-3 py-3 placeholder-black text-black bg-white text-md shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150" value="{{ $book->isbn }}">
                        @error('isbn')
                            <span>{{$message}}</span>
                        @enderror
                    </div>
                    </div>
                    <div class="w-full lg:w-6/12 px-2">
                      <div class="relative w-full mb-3">
                        <label class="block uppercase text-black font-bold mb-2">
                          Published Year
                        </label>
                        <input name="publishedYear" type="text" class="border border-black px-3 py-3 placeholder-black text-black bg-white text-md shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150" value="{{ $book->publishedYear }}">
                        @error('publishedYear')
                            <span>{{$message}}</span>
                        @enderror
                    </div>
                    </div>
                    <div class="w-full lg:w-6/12 px-2">
                        <div class="relative w-full mb-3">
                          <label class="block uppercase text-black font-bold mb-2">
                            Quantity
                          </label>
                          <input name="quantity" type="number" class="border border-black px-3 py-3 placeholder-black text-black bg-white text-md shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150" value="{{ $book->quantity }}">
                          @error('quantity')
                            <span>{{$message}}</span>
                        @enderror
                        </div>
                    </div>
                    <div class="w-full lg:w-6/12 px-2">
                        <div class="relative w-full mb-3">
                            <label class="block uppercase text-black font-bold mb-2" for="category">
                                Category
                              </label>
                            <select id="categories" name="categories_id" class="border border-black px-3 py-3 placeholder-black text-black bg-white text-md shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150">
                                @foreach ($categories as $item)
                                    @if ($book->categories_id === $item->id)
                                    <option value="{{$item->id}}" selected='selected' class="py-3">{{$item->name}}</option>
                                    @else
                                        <option value="{{$item->id}}" class="py-3">{{$item->name}}</option>
                                    @endif
                                @endforeach
                            </select>
                          </div>
                    </div>
                  </div>
                  <div class="flex flex-wrap">
                    <div class="w-full lg:w-12/12 px-2">
                      <div class="relative w-full mb-3">
                        <label class="block uppercase text-black font-bold mb-2">
                          Description
                        </label>
                        <textarea name="description" type="text" class="border border-black px-3 py-3 placeholder-black text-black bg-white text-md shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150" rows="4">{{$book->description}}</textarea>
                      </div>
                    </div>
                  </div>
                  <div class="flex justify-end px-2">
                    <button type="submit" class="bg-black text-white px-6 py-3 hover:bg-slate-800 shadow-slate-500/50">Update</button>
                  </div>
                </form>
              </div>
        </div>
    </section>
@endsection