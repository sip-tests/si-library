@extends('admin.layouts.index')

@section('content')
    <section class="px-5 md:px-0 w-full md:w-11/12 mx-auto">
        <div class="container mx-auto md:w-11/12">
            <div class="flex justify-between items-center py-3 pb-1">
                <h1 class="text-2xl py-5 font-medium">Books</h1>
                <div class="flex justify-end px-2">
                    <a href="/admin/books/create" class=" text-white hover:bg-slate-800 shadow-slate-500/50"><span class="bg-black px-6 py-2 hover:bg-slate-700 flex justify-center items-center">Add book</span></a>
                </div>
            </div>
            <div class="flex flex-col">
                @if(session('Error'))
                    <div
                        class="mb-4 rounded-lg bg-red-100 px-6 py-3 text-base text-red-700"
                        role="alert">
                        {{ session('Error') }}
                    </div>
                @endif
    
                @if(session('Success'))
                    <div
                        class="mb-4 rounded-lg bg-green-100 px-6 py-3 text-base text-green-700"
                        role="alert">
                        {{ session('Success') }}
                    </div>
                @endif
                <div class="overflow-x-auto sm:-mx-6 lg:-mx-8">
                    <div class="w-fit mx-auto py-2 px-5 lg:px-0" >
                        <div class="overflow-hidden border border-slate-200">
                            <table class="min-w-full text-center text-md w-full" >
                                <thead class="border-b bg-black text-white">
                                    <tr>
                                        <th scope="col" class="px-6 py-4">No</th>
                                        <th scope="col" class="px-6 py-4">Title</th>
                                        <th scope="col" class="px-6 py-4">Quantity</th>
                                        <th scope="col" class="px-6 py-4">Author</th>
                                        <th scope="col" class="px-6 py-4">Category</th>
                                        <th scope="col" class="px-6 py-4">Status</th>
                                        <th scope="col" class="px-6 py-4">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($books as $book)    
                                        <tr class="border-b">
                                            <td class="px-6 py-4 font-medium">{{ $loop->iteration }}</td>
                                            <td class="px-6 py-4 text-left">{{$book->title}}</td>
                                            <td class="px-6 py-4">{{$book->quantity}}</td>
                                            <td class="px-6 py-4">{{$book->author}}</td>
                                            <td class="w-fit px-6 py-4">{{$book->categories->name}}</td>
                                            <td class="px-6 py-4 font-semibold text-{{ $book->quantity > 0 ? 'green' : 'red' }}-500">
                                                <span class="bg-{{ $book->quantity > 0 ? 'green' : 'red' }}-100 px-4 py-1 rounded-md">
                                                    {{ $book->quantity > 0 ? 'Available' : 'Unavailable' }}
                                                </span>
                                            </td>
                                            <td class="px-6 py-4">
                                                <div class="flex justify-center gap-3">
                                                        <a href="/admin/books/{{$book->id}}/edit" class="hover:bg-yellow-300 bg-yellow-200 p-2 border border-yellow-700">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 24 24" fill="none" stroke="#a16207" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="lucide lucide-pencil">
                                                                <path d="M17 3a2.85 2.83 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5Z"/>
                                                                <path d="m15 5 4 4"/>
                                                            </svg>
                                                        </a>
                                                    <form action="{{ route('books.destroy', ['book' => $book]) }}" method="POST">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button type="submit" class="hover:bg-red-300 bg-red-200 p-2 border border-red-700"  onclick="return confirm('Are you sure you want to delete this book?');">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 24 24" fill="none" stroke="#b91c1c" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="lucide lucide-trash-2">
                                                                <path d="M3 6h18"/>
                                                                <path d="M19 6v14c0 1-1 2-2 2H7c-1 0-2-1-2-2V6"/>
                                                                <path d="M8 6V4c0-1 1-2 2-2h4c1 0 2 1 2 2v2"/>
                                                                <line x1="10" x2="10" y1="11" y2="17"/>
                                                                <line x1="14" x2="14" y1="11" y2="17"/>
                                                            </svg>
                                                        </button>
                                                    </form>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection