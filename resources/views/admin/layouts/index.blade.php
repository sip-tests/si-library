<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" type="text/css" href="{{ url('/css/style.css') }}" />
  @vite('resources/css/app.css')
</head>
<body>
    {{-- navbar --}}
    <nav class="relative w-full md:w-11/12 mx-auto px-5 md:px-0 py-4 flex justify-between items-center bg-white">
		<a class="text-2xl font-bold leading-none" href="#">
			Admin
		</a>
		<div class="lg:hidden">
			<button class="navbar-burger flex items-center text-black p-3">
				<svg class="block h-5 w-5 fill-current" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
					<title>Mobile menu</title>
					<path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z"></path>
				</svg>
			</button>
		</div>
		<ul class="hidden absolute top-1/2 left-1/2 transform -translate-y-1/2 -translate-x-1/2 lg:mx-auto lg:flex lg:items-center lg:w-auto lg:space-x-6">
			<li><a class="text-black hover:text-gray-500 " href="{{route('admin.home')}}">Home</a></li>
			<li><a class="text-black hover:text-gray-500 " href="{{route('admin.books')}}">Books</a></li>
			<li><a class="text-black hover:text-gray-500 " href="{{route('admin.members')}}">Member</a></li>
		</ul>
		<form class="hidden lg:inline-block"  action="{{route('logout')}}" method="post">
			@csrf
			<button class="py-2 px-6 bg-white border border-black hover:bg-black hover:text-white text-sm text-black font-bold transition duration-200 shadow-lg shadow-slate-500/50">Logout</button>
		</form>
	</nav>
	<div class="navbar-menu relative z-50 hidden">
		<div class="navbar-backdrop fixed inset-0 bg-gray-800 opacity-25"></div>
		<nav class="fixed top-0 left-0 bottom-0 flex flex-col w-5/6 max-w-sm py-6 px-6 bg-white border-r overflow-y-auto">
			<div class="flex items-center mb-8">
				<a class="mr-auto text-3xl font-bold leading-none" href="#">
					Admin
				</a>
				<button class="navbar-close">
					<svg class="h-6 w-6 text-gray-400 cursor-pointer hover:text-gray-500" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
						<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12"></path>
					</svg>
				</button>
			</div>
			<div>
				<ul>
					<li class="mb-1">
						<a class="block p-4 text-sm font-semibold text-gray-400 hover:bg-slate-50 hover:text-slate-600 rounded"  href="{{route('admin.home')}}">Home</a>
					</li>
					<li class="mb-1">
						<a class="block p-4 text-sm font-semibold text-gray-400 hover:bg-slate-50 hover:text-slate-600 rounded"  href="{{route('admin.books')}}">Books</a>
					</li>
					<li class="mb-1">
						<a class="block p-4 text-sm font-semibold text-gray-400 hover:bg-slate-50 hover:text-slate-600 rounded"  href="{{route('admin.members')}}">Member</a>
					</li>
				</ul>
			</div>
			<div class="mt-auto">
				<div class="pt-6">
					<form action="{{route('logout')}}" method="post">
						@csrf
						<button type="submit" class="block px-4 py-2 mb-2 leading-loose text-sm text-center text-white font-semibold bg-black hover:bg-black ">Logout</button>
					</form>
				</div>
			</div>
		</nav>
	</div>
    {{-- navbar end --}}
    <main>
        @yield('content')
    </main>

    
<script>
    // Burger menus
    document.addEventListener('DOMContentLoaded', function() {
        // open
        const burger = document.querySelectorAll('.navbar-burger');
        const menu = document.querySelectorAll('.navbar-menu');
    
        if (burger.length && menu.length) {
            for (var i = 0; i < burger.length; i++) {
                burger[i].addEventListener('click', function() {
                    for (var j = 0; j < menu.length; j++) {
                        menu[j].classList.toggle('hidden');
                    }
                });
            }
        }
    
        // close
        const close = document.querySelectorAll('.navbar-close');
        const backdrop = document.querySelectorAll('.navbar-backdrop');
    
        if (close.length) {
            for (var i = 0; i < close.length; i++) {
                close[i].addEventListener('click', function() {
                    for (var j = 0; j < menu.length; j++) {
                        menu[j].classList.toggle('hidden');
                    }
                });
            }
        }
    
        if (backdrop.length) {
            for (var i = 0; i < backdrop.length; i++) {
                backdrop[i].addEventListener('click', function() {
                    for (var j = 0; j < menu.length; j++) {
                        menu[j].classList.toggle('hidden');
                    }
                });
            }
        }
    });
    </script>
</body>
</html>