@extends('admin.layouts.index')

@section('content')
    <section class="px-5 md:px-0 w-full md:w-11/12 mx-auto">
        <div class="container mx-auto md:w-11/12">
            <div class="flex-auto px-2 lg:px-10 py-10 pt-0">
                <h1 class="text-xl py-5 font-medium">Edit Member</h1>
                <form method="POST" action="{{ route('members.update', $member->id) }}">
                    @csrf
                    @method('PUT')
                  <div class="flex flex-wrap">
                    <div class="w-full lg:w-6/12 px-2">
                        <div class="relative w-full mb-3">
                          <label class="block uppercase text-black font-bold mb-2">
                            Name
                          </label>
                          <input name="name" type="name" class="border border-black px-3 py-3 placeholder-black text-black bg-white text-md shadow focus:outline-none focus:ring w-full" value="{{$member->name}}">
                          @error('name')
                              <span>{{$message}}</span>
                          @enderror
                      </div>
                      </div>
                    <div class="w-full lg:w-6/12 px-2">
                      <div class="relative w-full mb-3">
                        <label class="block uppercase text-black font-bold mb-2">
                          Email
                        </label>
                        <input name="email" type="email" class="border border-black px-3 py-3 placeholder-black text-black bg-white text-md shadow focus:outline-none focus:ring w-full " value="{{$member->email}}">
                        @error('email')
                            <span>{{$message}}</span>
                        @enderror
                    </div>
                    </div>
                    <div class="w-full lg:w-6/12 px-2">
                      <div class="relative w-full mb-3">
                        <label class="block uppercase text-black font-bold mb-2">
                          Address
                        </label>
                        <input name="address" type="text" class="border border-black px-3 py-3 placeholder-black text-black bg-white text-md shadow focus:outline-none focus:ring w-full" value="{{$member->address}}">
                        @error('address')
                            <span>{{$message}}</span>
                        @enderror
                    </div>
                    </div>
                    <div class="w-full lg:w-6/12 px-2">
                      <div class="relative w-full mb-3">
                        <label class="block uppercase text-black font-bold mb-2">
                          Phone
                        </label>
                        <input name="phone" type="text" class="border border-black px-3 py-3 placeholder-black text-black bg-white text-md shadow focus:outline-none focus:ring w-full" value="{{$member->phone}}">
                        @error('phone')
                            <span>{{$message}}</span>
                        @enderror
                    </div>
                    </div>
                  </div>
                    <div class="flex justify-end px-2 pt-4">
                        <button type="submit" class="bg-black text-white px-6 py-3 hover:bg-slate-800 shadow-slate-500/50">Edit</button>
                    </div>
                </form>
              </div>
        </div>
    </section>
@endsection