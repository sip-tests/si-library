<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\BookController;
use App\Http\Controllers\BorrowController;
use App\Http\Controllers\MemberController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});

Route::controller(AuthController::class)->group(function(){
    Route::get('/login','login')->name('login')->middleware('guest');
    Route::get('/register','register')->name('register')->middleware('guest');
    Route::post('/authRegister','authRegister')->name('authRegister')->middleware('guest');
    Route::post('/logout','logout')->name('logout')->middleware('auth');
    Route::post('/authenticate','authenticate')->name('authenticate');
});

Route::prefix('admin')->middleware(['auth','user-access'])->group(function(){
    Route::get('/home',[AdminController::class, 'home'])->name('admin.home');

    Route::get('/books',[BookController::class, 'index'])->name('admin.books');
    Route::get('/books/{id}/edit',[BookController::class, 'edit'])->name('books.edit');
    Route::get('/books/{id}/details',[BookController::class, 'details'])->name('books.details');
    Route::put('/books/{id}', [BookController::class, 'update'])->name('books.update');
    Route::get('/books/create', [BookController::class, 'create'])->name('books.create');
    Route::post('/books/store', [BookController::class, 'store'])->name('books.store');
    Route::delete('/books/{book}', [BookController::class, 'destroy'])->name('books.destroy');

    Route::get('/members',[MemberController::class, 'index'])->name('admin.members');
    Route::get('/members/create',[MemberController::class, 'create'])->name('members.create');
    Route::post('/members/store', [MemberController::class, 'store'])->name('members.store');
    Route::get('/members/{id}/edit',[MemberController::class, 'edit'])->name('members.edit');
    Route::put('/members/{id}', [MemberController::class, 'update'])->name('members.update');
    Route::delete('/members/{member}', [MemberController::class, 'destroy'])->name('members.destroy');
});

Route::prefix('visitor')->middleware('auth')->group(function(){
    Route::get('/home',[UserController::class, 'home'])->name('visitor.home');
    Route::get('/{user}/my-books',[UserController::class, 'myBooks'])->name('visitor.my-books');
    Route::get('/profile',[UserController::class, 'profile'])->name('visitor.profile');

    Route::post('borrow/{book}', [BorrowController::class, 'borrowBook'])->name('borrowBook');
    Route::post('return/{book}', [BorrowController::class, 'returnBook'])->name('returnBook');

    Route::get('/profile/{id}/edit',[UserController::class, 'editProfile'])->name('profile.edit');
    Route::put('/profile/{id}', [UserController::class, 'updateProfile'])->name('profile.update');

    Route::get('/profile/{id}/change-password',[UserController::class, 'editPassword'])->name('profile.editPassword');
    Route::put('/profile/changepassword/{id}', [UserController::class, 'updatePassword'])->name('profile.updatePassword');
});
