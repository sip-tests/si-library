# Sistem Informasi Library (CRUD Laravel) - SIP Test

Website sistem informasi yang dibuat dengan framework laravel (PHP) yang mengimplementasi CRUD dengan database MySQL

## Tech Stack

**Database:** MySQL

**Webiste:** PHP, Laravel 10, TailwindCSS

## Installation

### Clone dan masuk ke repository

```bash
  git clone https://gitlab.com/sip-tests/si-library.git
  cd si-library
```

### Setting env

Buat file .env

-   Buat file .env dan salin isi .env_example ke .env

atau jalankan perintah

```bash
  cp .env_example .env
```

### Atur database di env

```bash
DB_DATABASE=db_si_library
DB_USERNAME=root
DB_PASSWORD=
```

### Buat database

-   buat database dengan nama db_si_library
-   import file db_si_library.sql ke dalam database db_si_library

### Instalasi dependensi

```bash
composer install
```

```bash
npm install vite
```

```bash
php artisan key:generate
```

## Running aplikasi

Jalankan aplikasi dengan 2 terminal :
Terimal 1 (Running laravel):

```bash
php artisan serve
```

Terimal 2 (Running npm):

```bash
npm run dev
```
