<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $fillable = ['title', 'author','cover','categories_id', 'isbn', 'publishedYear', 'quantity', 'description'];

    public function categories() {
        return $this->belongsTo(Category::class, 'categories_id');
    }

    public function borrows() {
        return $this->hasMany(Borrow::class, 'book_id');
    }
}
