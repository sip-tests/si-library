<?php

namespace App\Http\Controllers;

use App\Models\Book;
use Illuminate\Support\Facades\Auth;
use App\Models\Borrow;
use Illuminate\Http\Request;

class BorrowController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }
    public function borrowBook(Book $book)
    {
        $userId = Auth::id();

        $alreadyBorrowed = Borrow::where('user_id', $userId)
            ->where('book_id', $book->id)
            ->where('status', 'borrowed')
            ->exists();

        if ($alreadyBorrowed) {
            return redirect()->route('visitor.home')->with('Error', "You've already borrowed this book");
        }

        $borrow = new Borrow([
            'user_id' => $userId,
            'book_id' => $book->id,
            'borrowDate' => now(),
            'returnDate' => now()->addDays(14),
            'status' => 'borrowed',
        ]);

        $borrow->save();

        $book->decrement('quantity');

        return redirect()->route('visitor.home')->with('Success', "You've successfully borrow a book!");
    }

    public function returnBook($id)
    {
        $userId = Auth::id();

        $borrowedBook = Borrow::where('user_id', $userId)
            ->where('book_id', $id)
            ->where('status', 'borrowed')
            ->first();
        if (!$borrowedBook) {
            return redirect()->route('visitor.my-books', ['user' => $userId])->with('Error', "You are not borrowing the book!");
        }
        
        $borrowedBook->status = 'returned';
        $borrowedBook->actualReturnDate = now();
        $borrowedBook->save();

        $book = Book::find($id);

        if ($book) {
            $book->increment('quantity');
        }
        return redirect()->route('visitor.my-books', ['user' => $userId])->with('Success', "You have successfully returned the book!");

    }


    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Borrow $borrow)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Borrow $borrow)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Borrow $borrow)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Borrow $borrow)
    {
        //
    }
}
