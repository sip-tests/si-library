<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Models\Borrow;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        
    }
    public function home(Request $request)
    {
        //
        $query = $request->input('query');
        $books = Book::where('title', 'LIKE', "%$query%")
            ->orWhere('author', 'LIKE', "%$query%")
            ->latest()
            ->paginate(12);
        $count = Book::count();
        $userId = Auth::id();
        // $books = Book::limit(5)->get();
    
        return view('visitor.home.index', compact('userId','books'));
    }
    public function myBooks(User $user)
    {
        $userId = Auth::id();
        $borrowedBooks = $user->borrows()->where('status', 'borrowed')
            ->with(['book' => function ($query) {
                $query->select('id', 'title', 'author', 'isbn', 'cover', 'publishedYear', 'quantity', 'description');
            }]) 
            ->get();
        return view('visitor.my-books.index', compact('userId','borrowedBooks'));
    }
    public function profile(User $user)
    {
        $userId = Auth::id();
        $profile = User::where('id', $userId)->get();
        $borrowHistory = Borrow::where('status', 'returned')->where('user_id',$userId)
            ->with(['book' => function ($query) {
                $query->select('id', 'title', 'author', 'isbn', 'cover', 'publishedYear', 'quantity', 'description');
            }]) 
            ->orderBy('updated_at', 'desc')
            ->get();
        return view('visitor.profile.index',compact('userId','profile','borrowHistory'));
    }

    public function editProfile($id)
    {
        $profile = User::find($id);
        if(!$profile){
            return redirect()->route('visitor.profile')->with('Error', "Profile data unavailable");
        }
        return view('visitor.profile.edit.index',['userId'=>$id,'profile'=>$profile]);
        
    }
    public function updateProfile(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|max:255',
            'address' => 'required|string|max:255',
            'phone' => 'required|string|max:255',
        ]);

        $profile = User::find($id);

        if (!$profile) {
            return redirect()->route('visitor.profile')->with('Error', "Profile not found");
        }
        $profile->name = $request->input('name');
        $profile->email = $request->input('email');
        $profile->address = $request->input('address');
        $profile->phone = $request->input('phone');

        $profile->save();

        return redirect()->route('visitor.profile')->with('Success', "Profile updated successfully");
        
    }
    public function editPassword($id)
    {
        $profile = User::find($id);
        if(!$profile){
            return redirect()->route('visitor.profile')->with('Error', "User data unavailable");
        }
        return view('visitor.profile.change-password.index',['userId'=>$id,'profile'=>$profile]);
    }
    public function updatePassword(Request $request, $id)
    {
        $request->validate([
            'old_password' => 'required',
            'new_password' => 'required',
            'verify_password' => 'required|same:new_password',
        ]);

        $user = User::find($id);
        if (!$user) {
            return redirect()->route('visitor.profile')->with('Error', 'User not found.');
        }
        if (!Hash::check($request->input('old_password'), $user->password)) {
            return redirect()->back()->with('Error', 'Current password is incorrect.');
        }
        if ($request->input('new_password') === $request->input('verify_password')) {
            $user->password = Hash::make($request->input('new_password'));
            $user->save();
            return redirect()->route('visitor.profile')->with('Success', 'Password updated successfully.');
        } else {
            return redirect()->back()->withInput()->with('Error', 'New password and verify password do not match');
        }
    }


    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
