<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class MemberController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
        $members = User::where('type', 'visitor')->get();
        $totalMember = User::where('type','visitor')->count();
        return view('admin.member.index', compact('members','totalMember'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
        return view('admin.member.create.index');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|max:255',
            'address' => 'required|string|max:255',
            'phone' => 'required|string|max:255',
        ]);
        User::create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => Hash::make('12345'),
            'address' => $request->input('address'),
            'phone' => $request->input('phone'),
        ]);
        return redirect()->route('admin.members')->with('Success', "Member added successfully");
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        //
        $member = User::find($id);
        if(!$member){
            return redirect()->route('admin.members')->with('Error', "Member data unavailable");
        }
        return view('admin.member.edit.index',['member'=>$member]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|max:255',
            'address' => 'required|string|max:255',
            'phone' => 'required|string|max:255',
        ]);

        $member = User::find($id);

        if (!$member) {
            return redirect()->route('admin.members')->with('Error', "Member not found");
        }
        $member->name = $request->input('name');
        $member->email = $request->input('email');
        $member->address = $request->input('address');
        $member->phone = $request->input('phone');

        $member->save();

        return redirect()->route('admin.members')->with('Success', "Member updated successfully");
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
        $member = User::find($id);
        if(!$member){
            return redirect()->route('admin.members')->with('Error', "Member data unavailable");
        }
        $member->delete();
        return redirect()->route('admin.members')->with('Success', "Member deleted succesfully");
    }
}
