<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Models\Category;
use Illuminate\Http\Request;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
        $books = Book::orderBy('updated_at', 'desc')->get();
        return view('admin.book.index', compact('books'));
    }
    public function details($id){
        $books = Book::where('id', $id)->get();
        return view('admin.book.details.index', compact('books'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(Request $request)
    {
       return view('admin.book.create.index',['categories'=>Category::all()]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'title' => 'required|string|max:255',
            'author' => 'required|string|max:255',
            'isbn' => 'required|string|max:20',
            'publishedYear' => 'required|integer',
            'quantity' => 'required|integer',
            'categories_id' => 'required|integer',
            'description' => 'required|string',
            'cover' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
        ]);
        
        if($request->file('cover')){
            $file= $request->file('cover');
            $filename = $file->getClientOriginalName();
            $file-> move(public_path('public/cover'), $filename);
        }
        Book::create([
            'title' => $request->input('title'),
            'author' => $request->input('author'),
            'isbn' => $request->input('isbn'),
            'publishedYear' => $request->input('publishedYear'),
            'quantity' => $request->input('quantity'),
            'categories_id' => $request->input('categories_id'),
            'description' => $request->input('description'),
            'cover' => 'public/cover/'.$filename,
        ]);

        return redirect()->route('admin.books')->with('Success', "Book created successfully");
    }

    /**
     * Display the specified resource.
     */
    public function show(Book $book)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        //
        $book = Book::find($id);
        if(!$book){
            return redirect()->route('admin.books')->with('Error', "Book data unavailable");
        }
        return view('admin.book.edit.index',['book'=>$book,'categories'=>Category::all()]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required|string|max:255',
            'author' => 'required|string|max:255',
            'isbn' => 'required|string|max:20',
            'publishedYear' => 'required|integer',
            'quantity' => 'required|integer',
            'categories_id' => 'required|integer',
            'description' => 'required|string',
        ]);

        $book = Book::find($id);

        if (!$book) {
            return redirect()->route('admin.books')->with('Error', "Book not found");
        }
        $book->title = $request->input('title');
        $book->author = $request->input('author');
        $book->isbn = $request->input('isbn');
        $book->publishedYear = $request->input('publishedYear');
        $book->quantity = $request->input('quantity');
        $book->categories_id = $request->input('categories_id');
        $book->description = $request->input('description');

        $book->save();

        return redirect()->route('admin.books')->with('Success', "Book updated successfully");
    }

    public function destroy($id)
    {
        $book = Book::find($id);
        if(!$book){
            return redirect()->route('admin.books')->with('Error', "Book data unavailable");
        }
        $book->delete();
        return redirect()->route('admin.books')->with('Success', "Book deleted succesfully");
    }
}
