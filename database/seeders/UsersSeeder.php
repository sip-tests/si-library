<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        User::create([
            'name' => 'John Doe',
            'email' => 'admin@test.com',
            'password' => Hash::make('12345678'),
            'type' => 'admin',
            'address' => '123 Main St, Cityville',
            'phone' => '123-456-7890',
        ]);

        User::create([
            'name' => 'Jane Smith',
            'email' => 'visitor@test.com',
            'password' => Hash::make('12345678'),
            'type' => 'visitor',
            'address' => '456 Elm St, Townsville',
            'phone' => '987-654-3210',
        ]);
    }
}
