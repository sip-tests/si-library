<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use Faker\Factory as Faker;

class BookSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $faker = Faker::create();

        foreach (range(1, 25) as $index) { // Create 10 random book entries
            DB::table('books')->insert([
                'title' => $faker->sentence,
                'author' => $faker->name,
                'isbn' => $faker->isbn10,
                'cover' => $faker->imageUrl(200, 200, 'books'),
                'publishedYear' => $faker->year,
                'quantity' => $faker->numberBetween(1, 100),
                'categories_id' => $faker->numberBetween(1, 5),
                'description' => $faker->paragraph,
                'created_at' => now(),
                'updated_at' => now(),
            ]);
        }
    }
}
