-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 29, 2023 at 05:08 PM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 8.1.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_si_library`
--

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE `books` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `author` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `isbn` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cover` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `publishedYear` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `categories_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`id`, `title`, `author`, `isbn`, `cover`, `publishedYear`, `quantity`, `description`, `created_at`, `updated_at`, `categories_id`) VALUES
(28, 'Harry Potter', 'J.K Rowling', '123791074', 'public/cover/harrypotter.jpg', 1998, 5, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at sapien eget risus tristique varius. Proin vel tincidunt est. Phasellus ac justo id ligula condimentum venenatis. Nullam tincidunt ex ac orci iaculis, at malesuada neque congue. Sed eu mauris sed ex cursus eleifend. Cras at nisi in odio auctor bibendum.', '2023-10-28 19:37:39', '2023-10-29 04:50:46', 4),
(29, 'The Great Gatsby', 'F. Scoot Fitzgerald', '102374782', 'public/cover/greatgatsby.jpg', 1997, 4, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at sapien eget risus tristique varius. Proin vel tincidunt est. Phasellus ac justo id ligula condimentum venenatis. Nullam tincidunt ex ac orci iaculis, at malesuada neque congue. Sed eu mauris sed ex cursus eleifend. Cras at nisi in odio auctor bibendum. Fusce et tortor ac justo ullamcorper cursus. Praesent euismod neque at ex posuere, id suscipit nunc dictum. Aliquam erat volutpat. Duis feugiat facilisis volutpat. Maecenas eu urna semper, cursus ex et, tincidunt eros. Nullam consectetur risus eu elit varius, in euismod est facilisis. Phasellus eget odio a sapien venenatis scelerisque. Vivamus in vestibulum neque, et consectetur odio.', '2023-10-29 04:02:21', '2023-10-29 04:02:21', 1),
(30, 'The Sum of All Things', 'Nicole Brooks', '12389641284', 'public/cover/sumofall.jpeg', 2012, 3, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at sapien eget risus tristique varius. Proin vel tincidunt est. Phasellus ac justo id ligula condimentum venenatis. Nullam tincidunt ex ac orci iaculis, at malesuada neque congue. Sed eu mauris sed ex cursus eleifend. Cras at nisi in odio auctor bibendum. Fusce et tortor ac justo ullamcorper cursus. Praesent euismod neque at ex posuere, id suscipit nunc dictum. Aliquam erat volutpat. Duis feugiat facilisis volutpat. Maecenas eu urna semper, cursus ex et, tincidunt eros. Nullam consectetur risus eu elit varius, in euismod est facilisis. Phasellus eget odio a sapien venenatis scelerisque. Vivamus in vestibulum neque, et consectetur odio.', '2023-10-29 04:03:46', '2023-10-29 04:25:01', 1),
(31, 'Catch-22', 'Joshep Heller', '120418461', 'public/cover/catch22.jpg', 1998, 2, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at sapien eget risus tristique varius. Proin vel tincidunt est. Phasellus ac justo id ligula condimentum venenatis. Nullam tincidunt ex ac orci iaculis, at malesuada neque congue. Sed eu mauris sed ex cursus eleifend. Cras at nisi in odio auctor bibendum. Fusce et tortor ac justo ullamcorper cursus. Praesent euismod neque at ex posuere, id suscipit nunc dictum. Aliquam erat volutpat. Duis feugiat facilisis volutpat. Maecenas eu urna semper, cursus ex et, tincidunt eros. Nullam consectetur risus eu elit varius, in euismod est facilisis. Phasellus eget odio a sapien venenatis scelerisque. Vivamus in vestibulum neque, et consectetur odio.', '2023-10-29 04:05:04', '2023-10-29 04:24:25', 4);

-- --------------------------------------------------------

--
-- Table structure for table `borrows`
--

CREATE TABLE `borrows` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `borrowDate` date NOT NULL,
  `returnDate` date NOT NULL,
  `actualReturnDate` date DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `book_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `borrows`
--

INSERT INTO `borrows` (`id`, `borrowDate`, `returnDate`, `actualReturnDate`, `status`, `user_id`, `book_id`, `created_at`, `updated_at`) VALUES
(24, '2023-10-29', '2023-11-12', '2023-10-29', 'returned', 2, 28, '2023-10-28 19:59:35', '2023-10-28 20:01:33'),
(25, '2023-10-29', '2023-11-12', '2023-10-29', 'returned', 2, 28, '2023-10-28 22:47:39', '2023-10-28 22:47:44'),
(26, '2023-10-29', '2023-11-12', NULL, 'borrowed', 25, 31, '2023-10-29 04:24:25', '2023-10-29 04:24:25'),
(27, '2023-10-29', '2023-11-12', '2023-10-29', 'returned', 25, 28, '2023-10-29 04:24:35', '2023-10-29 04:24:45'),
(28, '2023-10-29', '2023-11-12', NULL, 'borrowed', 25, 30, '2023-10-29 04:25:01', '2023-10-29 04:25:01');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Fiction', NULL, NULL),
(2, 'Science Fiction', NULL, NULL),
(3, 'Mystery', NULL, NULL),
(4, 'Fantasy', NULL, NULL),
(5, 'Non-Fiction', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_reset_tokens_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2023_10_28_043639_create_books_table', 1),
(6, '2023_10_28_043751_create_categories_table', 1),
(7, '2023_10_28_043802_create_borrows_table', 1),
(8, '2023_10_28_053359_alter_books_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_reset_tokens`
--

CREATE TABLE `password_reset_tokens` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '12345',
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'visitor',
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `type`, `address`, `phone`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'John Doe', 'admin@test.com', '$2y$10$b./VH/xr9yonzcv6ujp/HujLhr0yC0GHePNnG8rqyS6D2Gj38NrqG', 'admin', '123 Main St, Cityville', '123-456-7890', NULL, '2023-10-28 07:01:49', '2023-10-28 07:01:49'),
(2, 'Alvin Fadli Dwi Mulya', 'visitor@test.com', '$2y$10$k2RRXrYOvUV3lRfH7l7V8uhC7uJsPnoi1O2f5.IlocvDdbl1RZFTu', 'visitor', 'Begischer Hof, Haupstasse', '987-654-3212', NULL, '2023-10-28 07:01:49', '2023-10-29 03:59:22'),
(25, 'Normal Person', 'visitor@mail.com', '$2y$10$dwTqKrQeIboVtE.0YHXO6uqGVoBDzaeHSMvhIKjaQBtKLKMv367FO', 'visitor', 'Gedebage, Bandung', '01231879843', NULL, '2023-10-29 04:23:48', '2023-10-29 04:25:46');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`id`),
  ADD KEY `books_categories_id_foreign` (`categories_id`);

--
-- Indexes for table `borrows`
--
ALTER TABLE `borrows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `borrows_user_id_foreign` (`user_id`),
  ADD KEY `borrows_book_id_foreign` (`book_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_reset_tokens`
--
ALTER TABLE `password_reset_tokens`
  ADD PRIMARY KEY (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `books`
--
ALTER TABLE `books`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `borrows`
--
ALTER TABLE `borrows`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `books`
--
ALTER TABLE `books`
  ADD CONSTRAINT `books_categories_id_foreign` FOREIGN KEY (`categories_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `borrows`
--
ALTER TABLE `borrows`
  ADD CONSTRAINT `borrows_book_id_foreign` FOREIGN KEY (`book_id`) REFERENCES `books` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `borrows_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
